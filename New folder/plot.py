import json
import pylab
import numpy as np
import array
import matplotlib.pyplot as plt
import sys
import math

f = open(sys.argv[1], 'r')

timestamp = []
ext_accel_x = []
ext_accel_y = []
ext_accel_z = []
ext_accel = []
int_accel_x = []
int_accel_y = []
int_accel_z = []
int_accel = []
light = []
sound = []


for line in f:
	print line
	if "{" in line:
		data = json.loads(line)
		timestamp.append(data['time'])
		ext_accel_x.append(data['exacc'][0])
		ext_accel_y.append(data['exacc'][1])
		ext_accel_z.append(data['exacc'][2])
		ext_accel.append(math.sqrt(math.pow(data['exacc'][0],2)+math.pow(data['exacc'][1],2)+math.pow(data['exacc'][2],2)))
		int_accel_x.append(data['inacc'][0])
		int_accel_y.append(data['inacc'][1])
		int_accel_z.append(data['inacc'][2])
		int_accel.append(math.sqrt(math.pow(data['inacc'][0],2)+math.pow(data['inacc'][1],2)+math.pow(data['inacc'][2],2)))
		light.append(data['light'])
		sound.append(data['sound'])
		if data['position'] == 0:
			positionBack+=1
		elif data['position'] == 1:
			positionSide+=1
		else:
			positionStom+=1

t = np.arange(0, len(timestamp), 1)

plt.figure()
plt.title('ext_accel')
plt.plot(t, ext_accel_x, 'r', t, ext_accel_y, 'g', t, ext_accel_z, 'b', t, ext_accel, 'black')

plt.figure()
plt.title('int_accel')
plt.plot(t, int_accel_x, 'r', t, int_accel_y, 'g', t, int_accel_z, 'b', t, int_accel, 'black')

plt.figure()
plt.title('light')
plt.plot(t, light, 'b')

plt.figure()
plt.title('sound')
plt.plot(t, sound, 'b')

plt.show()
