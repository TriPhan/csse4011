package ioio.examples.hello;



import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;


/*private float[] ext_accel_x ;
private float[] ext_accel_y ;
private float[] ext_accel_z;
private float[] ext_accel ;
private float[] int_accel_x ;
private float[] int_accel_y ;
private float[] int_accel_z ;
private float[] int_accel ;
private float[] light ;
private float[] sound ;
private ArrayList<Integer> ext_accel_thres ;
private ArrayList<Integer> ext_accel_thres_sum ;
private ArrayList<Integer> standing;*/
public class GraphActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.graph);
		
		//retrieve information
		Bundle extras = getIntent().getExtras();
		
		
		GraphView mView = new GraphView(this,extras.getFloatArray("ext_accel_x"),
				extras.getFloatArray("ext_accel_y"),extras.getFloatArray("ext_accel_z"),
				extras.getFloatArray("ext_accel"),extras.getFloatArray("int_accel_x"),
				extras.getFloatArray("int_accel_y"),extras.getFloatArray("int_accel_z"),
				extras.getFloatArray("int_accel"),extras.getFloatArray("light"),
				extras.getFloatArray("sound"),extras.getIntegerArrayList("ext_accel_thres"),
				extras.getIntegerArrayList("ext_accel_thres_sum"),
				extras.getIntegerArrayList("standing"),extras.getInt("graphOption"),
				extras.getFloat("lightLimit"),extras.getFloat("soundLimit"));
		
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(mView);
	}
	
	protected void onResume() {
  		super.onResume();
	}
	
	protected void onPause() {
  		super.onPause();
	}

}
