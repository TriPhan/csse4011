package ioio.examples.hello;



import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.*;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import ioio.lib.api.DigitalOutput;
import ioio.lib.api.AnalogInput;
import ioio.lib.api.DigitalInput;
import ioio.lib.api.TwiMaster;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOActivity;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * This is the main activity of the HelloIOIO example application.
 * 
 * It displays a toggle button on the screen, which enables control of the
 * on-board LED. This example shows a very simple usage of the IOIO, by using
 * the {@link IOIOActivity} class. For a more advanced use case, see the
 * HelloIOIOPower example.
 */
public class MainActivity extends IOIOActivity implements SensorEventListener {
	private TextView accelData;
	private Sensor mAccelerometer;
	private SensorManager mSensorManager;
	private boolean stand = false;
	
	
	private Button startButton;
	private Button stopButton;
	private Button clearButton;
	private Button showButton;
	private Button graphButton;
	private Button analyzeButton;
	private Button sendButton;
	
	//SD card logging
	private FileIO fileIO;
	
	
	//data and clk pins
	private DigitalOutput data;
	private DigitalOutput clk;
	private AnalogInput light_;
	private AnalogInput sound_;
	
	private int RedVal = 0;
	private int GreenVal = 0;
	private int BlueVal = 0;
	
	private float lightVal = 0;
	private float soundVal = 0;
	
	private float[] accel_val = {0,0,0};
	private int[] accel_val_int = {0,0,0};
	private float[] value={0,0,0};
	private int startFlag = 0;
	private int showData = 0;
	
	private TextView lightText_;
	private TextView soundText_;
	private SeekBar lightSeek_;
	private SeekBar soundSeek_;
	private float lightLimit = 1.5f;
	private float soundLimit = 1.5f;
	
	private Spinner rateSpinner_;
	private Spinner fileSpinner_;
	private Spinner graphSpinner_;
	
	private int userRating = 0;//0: non-selected 1:Poor 2:Okay 3:Good
	private int graphOption = 0;//0:ext+stand 1:int+stand 2:ext+int+stand 3:ext+int+thres+thressum
								//4:light/sound + limit 5:all
	
	
	//run time got ups counting
	private int numGetUps = 0;
	
	
	//sleepPos 0:back 1:side 2:stomach 3:not lying 
	private int sleepPos = 0;
	
	//lists containing data
	private List<String> timestamp ;
	
	
	private List<Float> ext_accel_x ;
	private List<Float> ext_accel_y ;
	private List<Float> ext_accel_z;
	private List<Float> ext_accel ;
	private List<Float> int_accel_x ;
	private List<Float> int_accel_y ;
	private List<Float> int_accel_z ;
	private List<Float> int_accel ;
	private List<Float> light ;
	private List<Float> sound ;
	private List<Integer> ext_accel_thres ;
	private List<Integer> ext_accel_thres_sum ;
	private List<Integer> standing;
	
	
	
	/**
	 * Called when the activity is first created. Here we normally initialize
	 * our GUI.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		this.accelData = (TextView)this.findViewById(R.id.accelData);
        accelData.setKeyListener(null);
        
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer=mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		
        
        Timer timer = new Timer();
        timer.schedule(new LogData(), 0,1000);
        
        
        graphButton = (Button) findViewById(R.id.graph);
        graphButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
            	Intent intent = new Intent(MainActivity.this, GraphActivity.class);
            	
            	
            	//pass information
            	//floats
            	intent.putExtra("ext_accel_x", convert(ext_accel_x));
            	intent.putExtra("ext_accel_y", convert(ext_accel_y));
            	intent.putExtra("ext_accel_z", convert(ext_accel_z));
            	intent.putExtra("ext_accel", convert(ext_accel));
            	intent.putExtra("int_accel_x", convert(int_accel_x));
            	intent.putExtra("int_accel_y", convert(int_accel_y));
            	intent.putExtra("int_accel_z", convert(int_accel_z));
            	intent.putExtra("int_accel", convert(int_accel));
            	intent.putExtra("light", convert(light));
            	intent.putExtra("sound", convert(sound));
            	
            	//ints
            	intent.putIntegerArrayListExtra("ext_accel_thres",(ArrayList<Integer>) ext_accel_thres);
            	intent.putIntegerArrayListExtra("ext_accel_thres_sum",(ArrayList<Integer>) ext_accel_thres_sum);
            	intent.putIntegerArrayListExtra("standing",(ArrayList<Integer>) standing);
            	
            	intent.putExtra("graphOption", graphOption);
            	
            	intent.putExtra("lightLimit", lightLimit);
            	intent.putExtra("soundLimit", soundLimit);
            	
            	
            	MainActivity.this.startActivity(intent);
            }
            
            private float[] convert(List<Float> input){
            	float[] output = new float[input.size()];
            	for (int i = 0;i<input.size();i++){
            		output[i] = input.get(i);
            	}
            	return output;
            }
        });
        
        
        class LongTask extends AsyncTask<Void, Void, String>{
        	protected String doInBackground(Void... not){
        		String value = "";
        		try {
					value = analyzeData();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		return value;
        	}
        	
        	 @Override
             protected void onPostExecute(String result) {
        		 accelData.setText(result);
        	 }
        	
        }
        
        analyzeButton = (Button) findViewById(R.id.analyze);
        analyzeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
            	accelData.setText("analyzing "+fileIO.getFileName()+ "...\n");
            	new LongTask().execute();
            	
            }
        });
        
        
        
        showButton = (Button) findViewById(R.id.showLive);
        showButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
            	if (showData == 0){
            		showData = 1;
            		showButton.setBackgroundColor(Color.CYAN);
            	}else{
            		showData = 0;
            		accelData.setText("");
            		showButton.setBackgroundResource(android.R.drawable.btn_default);
            	}
            }
        });
        
		startButton = (Button) findViewById(R.id.start);
		startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
            	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            	startFlag = 1;
            	String dateStamp = (DateFormat.format("dd-MM-yyyy-hh-mm-ss", new java.util.Date()).toString()); 
                fileIO = new FileIO("/LOGGING",String.format("log-%s.txt",dateStamp));
            }
        });
		
		stopButton=(Button) findViewById(R.id.stop);
		stopButton.setOnClickListener(new View.OnClickListener() {
             public void onClick(View v) {
                 // Perform action on click
            	 getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            	 startFlag = 0;
            	 
            	 accelData.setText("stopped recording. The data was logged to\n" + 
            			 			 fileIO.getFileName() + "\n Please rate your sleep"
            			 			 		+ " before analyzing");
            	 updateFileSpinner();
            	 
             }
         });
		 
		 clearButton = (Button) findViewById(R.id.clear);
		 clearButton.setOnClickListener(new View.OnClickListener() {
             public void onClick(View v) {
                 // Perform action on click
            	 
            	 accelData.setText("");
            	 
             }
         });
		 
		 lightText_ = (TextView) findViewById(R.id.lightText);
		 soundText_ = (TextView) findViewById(R.id.soundText);
		 
		 lightSeek_ = (SeekBar)  findViewById(R.id.seekLight);	
	     soundSeek_ = (SeekBar)  findViewById(R.id.seekSound);
	     
	     lightSeek_.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
				
				public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser){
					lightLimit = ((float)progressValue)*((float)3)/((float)100);
					lightText_.setText("Light Limit\n"+ lightLimit + "/3.0");
				}
				public void onStartTrackingTouch(SeekBar seekBar){}
				public void onStopTrackingTouch(SeekBar seekBar){}
					
			});
	     
	     soundSeek_.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
				
				public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser){
					soundLimit = ((float)progressValue)*((float)3)/((float)100);
					soundText_.setText("Sound Limit\n"+ soundLimit + "/3.0");
				}
				public void onStartTrackingTouch(SeekBar seekBar){}
				public void onStopTrackingTouch(SeekBar seekBar){}
					
			});
	     
	     //rateSpinner
	     rateSpinner_ = (Spinner) findViewById(R.id.rateSpinner);
	     ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
	    	        R.array.rateArray, android.R.layout.simple_spinner_item);
	     adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     rateSpinner_.setAdapter(adapter);
	     rateSpinner_.setOnItemSelectedListener(new CustomSpinnerListener());
	     
	     //graphSpinner
	     graphSpinner_ = (Spinner) findViewById(R.id.graphSpinner);
	     ArrayAdapter<CharSequence> graphAdapter = ArrayAdapter.createFromResource(this,
	    	        R.array.graphArray, android.R.layout.simple_spinner_item);
	     graphAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     graphSpinner_.setAdapter(graphAdapter);
	     graphSpinner_.setOnItemSelectedListener(new CustomGraphSpinnerListener());
	     
	     
	     //fileSpinner
	     fileSpinner_ = (Spinner) findViewById(R.id.fileSpinner);
	     updateFileSpinner();
	     fileSpinner_.setOnItemSelectedListener(new CustomFileSpinnerListener());
	     
	     sendButton = (Button) findViewById(R.id.sendEmail);
	     sendButton.setOnClickListener(new View.OnClickListener() {
             public void onClick(View v) {
                 // Perform action on click
            	 try 
            	   {   
            		switch (userRating) {
					case 0:
						fileIO.writeToFile("{\"rating\":Not rated yet}\r\n");
						break;
					case 1:
						fileIO.writeToFile("{\"rating\":Poor}\r\n");
						break;
					case 2:
						fileIO.writeToFile("{\"rating\":Okay}\r\n");
						break;
					case 3:
						fileIO.writeToFile("{\"rating\":Good}\r\n");
						break;
					default:
						break;
					}
            	       String fileName = URLEncoder.encode(fileIO.getFileName(), "UTF-8");
            	       String PATH =  Environment.getExternalStorageDirectory().getAbsolutePath()
            	    		   +"/LOGGING/"+fileIO.getFileName();

            	       Uri uri = Uri.parse("file://"+PATH);
            	       Intent i = new Intent(Intent.ACTION_SEND);
            	       i.setType("text/plain");
            	       i.putExtra(Intent.EXTRA_EMAIL, "");
            	       i.putExtra(Intent.EXTRA_SUBJECT,fileIO.getFileName());
            	       i.putExtra(Intent.EXTRA_TEXT,"");
            	       i.putExtra(Intent.EXTRA_STREAM, uri);
            	       MainActivity.this.startActivity(Intent.createChooser(i, "Select application"));
            	   } 
            	   catch (UnsupportedEncodingException e) 
            	   {
            	        // TODO Auto-generated catch block
            	        e.printStackTrace();
            	   }
            	 
             }
         });
	     
			
			
	}
	
	private void updateFileSpinner(){
		List<String> items = new ArrayList<String>();
	     File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/LOGGING");
	     File file[] = dir.listFiles();
	     for (int i = 0; i < file.length;i++){
	    	 items.add(file[i].getName());
	     }
	     ArrayAdapter<String> fileList = new ArrayAdapter<String>(this,
	                R.layout.row, items);
	     fileSpinner_.setAdapter(fileList);
	}
	
	class CustomGraphSpinnerListener implements OnItemSelectedListener{

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			
			graphOption = position;
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {}
		
	}
	
	class CustomFileSpinnerListener implements OnItemSelectedListener{

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			
			fileIO = new FileIO("/LOGGING",parent.getItemAtPosition(position).toString());
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {}
		
	}
	
	class CustomSpinnerListener implements OnItemSelectedListener{

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			
			userRating = position;
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {}
		
	}
	
	protected void onResume() {
  		super.onResume();
  		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
  		
  	}

  	protected void onPause() {
  		super.onPause();
  		mSensorManager.unregisterListener(this);
  	}

  	public void onAccuracyChanged(Sensor sensor, int accuracy) {
  		
  		
  	}

  	public void onSensorChanged(SensorEvent event) {
  		
  		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
  			if (startFlag == 1){
  				getAccelerometer(event);
  			}
  	    }	
  	}
  	
  	private float[] extractList(String input){
  		float[] output = {0,0,0};
  		int first = input.indexOf(',');
  		int second = input.indexOf(',',first+1);
  		output[0] = Float.parseFloat(input.substring(1, first));
  		output[1] = Float.parseFloat(input.substring(first+1, second));
  		output[2] = Float.parseFloat(input.substring(second+1, input.length()-1));
  	
  		return output;
  	}
  	
  	
  	
  	private String analyzeData() throws JSONException{
  		//variables
  		int i = 0,j;
  		JSONObject object;
  		StringBuilder message = new StringBuilder();
  		message.append("analyzing "+fileIO.getFileName()+ "...\n");
  		List<String> lines = fileIO.readFile();
  		
  		//lists containing data
  		timestamp = new ArrayList<String>();
  		ext_accel_x = new ArrayList<Float>();
  		ext_accel_y = new ArrayList<Float>();
  		ext_accel_z = new ArrayList<Float>();
  		ext_accel = new ArrayList<Float>();
  		int_accel_x = new ArrayList<Float>();
  		int_accel_y = new ArrayList<Float>();
  		int_accel_z = new ArrayList<Float>();
  		int_accel = new ArrayList<Float>();
  		light = new ArrayList<Float>();
  		sound = new ArrayList<Float>();
  		ext_accel_thres = new ArrayList<Integer>();
  		ext_accel_thres_sum = new ArrayList<Integer>();
  		standing = new ArrayList<Integer>();
  		int thres_fact = 5;
  		
  		int posBack = 0;
  		int posSide = 0;
  		int posStom = 0;
  		int posStand = 0;
  		float[] values;
  		
  		int avaiPos = 0;
  		
  		numGetUps = 0;
  		int prevPos = 0;
  		int currentPos = 0;
  		for (i = 0; i < lines.size();i++){
  			if (lines.get(i).contains("{") == true && lines.get(i).contains("rating") == false){//valid JSON
  				object = (JSONObject) new JSONTokener(lines.get(i)).nextValue();
  	  			if (i == 0){
  	  				if (lines.get(i).contains("position") == true){
  	  					avaiPos = 1;
  	  				}
  	  			}
  	  			//update lists
  	  			timestamp.add(object.getString("time"));
  	  		
  	  			
  	  			values = extractList(object.getString("exacc"));
  	  			ext_accel_x.add(values[0]);
  	  			ext_accel_y.add(values[1]);
  	  			ext_accel_z.add(values[2]);
  	  			ext_accel.add( (float) Math.sqrt( Math.pow(values[0],2) + Math.pow(values[1],2)
  	  					+ Math.pow(values[2],2) ) );
  	  			
  	  			values = extractList(object.getString("inacc"));
  	  			int_accel_x.add(values[0]);
  	  			int_accel_y.add(values[1]);
  	  			int_accel_z.add(values[2]);
  	  			int_accel.add( (float) Math.sqrt( Math.pow(values[0],2) + Math.pow(values[1],2)
  	  					+ Math.pow(values[2],2) ) );
  	  			
  	  			light.add((float) object.getDouble("light"));
  	  			sound.add((float) object.getDouble("sound"));
  	  			
  	  			if (avaiPos == 1){
  	  				currentPos = object.getInt("position");
  					switch (currentPos) {
  					case 0:
  						posBack++;
  						break;
  					case 1:
  						posSide++;
  						break;
  					case 2:
  						posStom++;
  						break;
  					case 3:
  						posStand++;
  						break;
  					default:
  						break;
  					}
  					
  					if (i != 0){
  						if (currentPos == 3 && prevPos != currentPos){
  							numGetUps++;
  						}	
  					}
  					prevPos = currentPos;
  	  			}
  	  			
  	  			ext_accel_thres.add(0);	
  	  			ext_accel_thres_sum.add(0);
  	  			standing.add(0);
  			}
  		}
  		
  		
  		
  		//section
  		i = 3;
		float thres = 0;
		float mean = 0;
		float mean_c = 0;
  		while (i < timestamp.size()){
  			thres = 0;
  			mean = 0;
  			mean_c = 0;
  			for (j = 2; j < 60;j++){
  				if ((i-j)>=0){
  					mean+=Math.abs(ext_accel_x.get(i-j+1)-ext_accel_x.get(i-j));
  					mean+= Math.abs(ext_accel_y.get(i-j+1)-ext_accel_y.get(i-j));
  					mean+= Math.abs(ext_accel_z.get(i-j+1)-ext_accel_z.get(i-j));
  					mean_c += 3;
  				}			
  			}
  			
  			thres = mean/mean_c;
  			if ((ext_accel_x.get(i)-ext_accel_x.get(i-1) > thres_fact*thres) 
  				|| (ext_accel_y.get(i)-ext_accel_y.get(i-1) > thres_fact*thres) 
  				|| (ext_accel_z.get(i)-ext_accel_z.get(i-1) > thres_fact*thres)){
  				ext_accel_thres.set(i, 1);
  			}
  			i++;
  		}
  		
  		//section
  		i = 1;
  		j = -100;
  		int flag = 0;
  		while (i < timestamp.size()){
  			ext_accel_thres_sum.set(i,ext_accel_thres_sum.get(i-1));
  			if ((ext_accel_thres.get(i) == 1) && ((i-j) >=100) ){
  				if (flag != 0){
  					ext_accel_thres.set(j+1, -1);
  				}
  				ext_accel_thres.set(i, 1);
  				flag = 1;
  				j = i;
  				ext_accel_thres_sum.set(i, ext_accel_thres_sum.get(i)+1);
  			}else if ((ext_accel_thres.get(i) == 1) && ((i-j) < 100) ){
  				if (flag == 0){
  					ext_accel_thres.set(j, 0);
  				}
  				ext_accel_thres.set(i, -1);
  				flag = 0;
  				j = i;
  			}
  			i++;
  		}
  		if (flag != 0){
  			ext_accel_thres.set(j+1, -1);
  		}
  		//plot ext_accel stuff here
  		
  		
  		//section
  		float percent = 0.5f;
  		for (i = 1; i < (timestamp.size()-1);i++){
  			if ((Math.abs(int_accel_y.get(i-1)) > (9.81*percent)) 
  					&& (Math.abs(int_accel_y.get(i)) > (9.81*percent)) 
  					&& (Math.abs(int_accel_y.get(i+1)) > (9.81*percent))){
  				standing.set(i, 1);
  				
  			}
  			
  		}
  		
  		//plot standing here
  		
  		//collect data for message
  		message.append("Detected the following events: \n");
  		i = 20;
  		int inbed = 0;
  		int count_rem = 0;
  		int index_entrance = -1;
		int index_exit = -1;
		int count_gotups = 0;
		String noise_limit = "No";
		String light_limit = "No";
		int quality = 0;
		while (i < (timestamp.size()-20)){
			if (ext_accel_thres.get(i) == 1){
				j = i+1;
				while (j < (timestamp.size()-20)){
					if (ext_accel_thres.get(j)==-1){
						if (standing.get(i-15)>standing.get(j+15)){
							if (index_entrance == -1){
								index_entrance = i;
							}else{
								count_gotups += 1;
							}
							message.append("Bed entrance: " + timestamp.get(i) + "\n");
							inbed = 1;
						}else if (standing.get(i-15) < standing.get(j+15)){
							index_exit = i;
							message.append("Bed exit: " + timestamp.get(i) + "\n");
							inbed = 0;
						}else{
							if ((j-i)==1){
								message.append("Unknown event (probably false-detection): "
										+ timestamp.get(i) + "\n");
			
							}else{
								if (standing.get(i-15) == 1 && inbed == 0){
									message.append("Made the bed: "
											+ timestamp.get(i) + "\n");
								}else if (standing.get(i-15) == 0 && inbed == 1){
									count_rem+=1;
									message.append("REM-phase: "
											+ timestamp.get(i) + "\n");
								}else{
									message.append("Did something weird: "
											+ timestamp.get(i) + "\n");
								}
							}
							
						}
						break;	
					}
					j+=1;	
				}
				i = j+1;
				
			}
			if (sound.get(i) > soundLimit){
				message.append("Sound limit exceeded: "
						+ timestamp.get(i) + "\n");
				noise_limit = "Yes";
				quality-=1;
			}
			if (light.get(i) > lightLimit){
				message.append("Light limit exceeded : "
						+ timestamp.get(i) + "\n");
				light_limit = "Yes";
				quality-=1;
			}
			i+=1;	
		}
  		
		message.append("\n");
  		message.append("Time in back position: ")
  		.append(Integer.toString(posBack/60) ).append(" minutes\n");
  		message.append("Time in side position: ")
  		.append(Integer.toString( posSide/60) ).append(" minutes\n");
  		message.append("Time in stomach position: ")
  		.append(Integer.toString( posStom/60) ).append(" minutes\n");
  		
  		int min = (posSide + posBack + posStom)/60;
  		int hr = min/60;
  		min = min%60;
  		message.append("\n")
  		.append("Sleep quality parameters: \n")
  		.append("Duration: "+ Integer.toString(hr) + " hour(s) ")
  		.append(Integer.toString(min) + " minute(s) \n")
  		.append("Number of Dectected Movements: " + Integer.toString(count_rem) +"\n")
  		//consider adding numGetUps/count_gotups here
  		.append("Number of get-ups: " + Integer.toString(numGetUps) + "\n")
  		.append("Sound limit exceeded: "+ noise_limit + "\n")
  		.append("Light limit exceeded: "+light_limit + "\n");
  		
  		if (hr >= 8){
  			quality += 1;
  		}
  		if (hr >= 7){
  			quality += 1;
  		}
  		if (count_rem >= 3){
  			quality += 1;
  		}
  		if (count_rem >= 5){
  			quality += 1;
  		}
  		
  		//count_gotups
  		if (numGetUps > 3){
  			quality -= 1;
  		}

  		String quality_str = "Poor";
  		if (quality >= -1){
  			quality_str = "Okay";
  		}
  		if (quality > 0){
  			quality_str = "Good";
  		}
  		message.append("Estimated Sleep Quality: " + quality_str + "\n");
  		message.append("You rated your sleep as: ");
  		switch(userRating){
  		case 0:
  			message.append("Not rated yet\n");
  			break;
  		case 1:
  			message.append("Poor\n");
  			break;
  		case 2:
  			message.append("Okay\n");
  			break;
  		case 3:
  			message.append("Good\n");
  			break;
  		default:
  			break;
  		}
  		
  		//accelData.setText(message.toString());
  		return message.toString();
  	}
  	
  	
  	
  	private void getAccelerometer(SensorEvent event){
  		float[] values = event.values;
  		value = values;
  		

  		if (showData == 1){
			accelData.setText("x: " + values[0] + "\n y: " + values[1]
					+ " \n z: " + values[2] + "\n light: " + lightVal
					+ "\n sound: " + soundVal + " Acceleration x: "
					+ accel_val[0] + "\n y: " + accel_val[1] + " \n z: "
					+ accel_val[2] + " \n sleepPos: " + sleepPos);
  		}
  		
  		if (values[1] < -8.5 && stand == false){
  			RedVal = 255;
  			BlueVal = 0;
  			GreenVal = 0;
  			stand = true;
  			
  			
  		}
  		
  		if (values[1] > -4 && stand == true){
  			RedVal = 0;
  			BlueVal = 0;
  			GreenVal = 0;
  			stand = false;
  		}
  		
  		if (stand == true){
  			RedVal = 255;
  			/*if (lightVal > 0.9){
  				RedVal = 0;
  			}else{
  				RedVal = 255;
  			}*/
  		}
  		
  		if (stand == true){
  			sleepPos = 3;
  		}else{	
  			if (values[2] < -8){
  				sleepPos = 2; //stomach
  			}else if (values[2] > -6){
  				if (values[0] > 8 || values[0] < -8){
  					sleepPos = 1;//side
  				}
  				if (values[0] < 6 && values[0] > -6){
  					sleepPos = 0;//back
  				}
  					
  			}
  					
  		}
  		
  		
  	}

  	class LogData extends TimerTask{
  		public void run(){
  			if (startFlag == 1){
				String dateStamp = (DateFormat.format("yyyyMMddhhmmss",
						new java.util.Date()).toString());

				fileIO.writeToFile(new StringBuilder().append("{\"time\":")
						.append(dateStamp).append(", \"exacc\":[")
						.append(accel_val[0]).append(",").append(accel_val[1])
						.append(",").append(accel_val[2])
						.append("],\"inacc\":[").append(value[0]).append(",")
						.append(value[1]).append(",").append(value[2])
						.append("],\"light\":").append(lightVal)
						.append(",\"sound\":").append(soundVal)
						.append(",\"position\":").append(sleepPos)
						.append("}\r\n").toString());
  			}
  		}
  	}
	/**
	 * This is the thread on which all the IOIO activity happens. It will be run
	 * every time the application is resumed and aborted when it is paused. The
	 * method setup() will be called right after a connection with the IOIO has
	 * been established (which might happen several times!). Then, loop() will
	 * be called repetitively until the IOIO gets disconnected.
	 */
	class Looper extends BaseIOIOLooper {
		/** The on-board LED. */
		private DigitalOutput led_;
		private DigitalInput pushBtn;
		
		/*external accelerometer functions*/
		private TwiMaster twi;
		
		
		
		
		
		byte LSM303_read(byte address) throws ConnectionLostException, InterruptedException	{
			byte result;
			byte[] receive_a = new byte[1]; 
			byte[] send_a = new byte[1];
			
			send_a[0] = address;
		  
			twi.writeRead(0x18, false, send_a, 1, receive_a, 1);
			
			result = receive_a[0];
			
		  
			return result;
		}

		void LSM303_write(byte data, byte address) throws ConnectionLostException, InterruptedException {

			byte[] receive_a = new byte[1]; 
			byte[] send_a = new byte[2];
			
			send_a[0] = address;
			send_a[1] = data;
		  
			twi.writeRead(0x18, false, send_a, 2, receive_a, 0);
			
		}
		
		void getLSM303_accel() throws ConnectionLostException, InterruptedException {
			accel_val_int[0] = -((int)LSM303_read((byte)0x29))*256+(LSM303_read((byte)0x28) & 0xff);
			accel_val[0] = 0.000627451f * accel_val_int[0];
		
			accel_val_int[1] = -((int)LSM303_read((byte)0x2B))*256+(LSM303_read((byte)0x2A) & 0xff);
			accel_val[1] = 0.000627451f * accel_val_int[1];
			
			accel_val_int[2] = -((int)LSM303_read((byte)0x2D))*256+(LSM303_read((byte)0x2C) & 0xff);
			accel_val[2] = 0.000627451f * accel_val_int[2];
		  // had to swap those to right the data with the proper axis
		}
		
		
		/*
		 * External functions for RGB LED
		 */
		
		void clock() throws ConnectionLostException{
			int i = 0;
		    clk.write(false);
		    for (i = 0; i < 20000; i++){};
		    clk.write(true);;
		    for (i = 0; i < 20000; i++){};
			
		}
		
		void sendByte(int dataIn) throws ConnectionLostException {
		     int i = 0;
		    
		    for (i=0; i<8;i++){
		          if ((dataIn & 0x80) != 0){
		               data.write(true);
		           }else{
		               data.write(false);
		           }
		           clock();
		           dataIn <<= 1;  
		     }  
		  }
		
		
		public void setColorRgb(int red,int green,int blue ) throws ConnectionLostException{
		
		    int prefix = 192;
		    if ((blue & 0x80) == 0) prefix|= 32;
		    if ((blue & 0x40) == 0) prefix|= 16;
		    if ((green & 0x80) == 0) prefix|= 8;
		    if ((green & 0x40) == 0) prefix|= 4;
		    if ((red & 0x80) == 0) prefix|= 2;
		    if ((red & 0x40) == 0) prefix|= 1;
		    
		    //send things
		    sendByte(0x00);
		    sendByte(0x00);
		    sendByte(0x00);
		    sendByte(0x00);
		    
		    
			sendByte(prefix);
			sendByte(blue);
			sendByte(green);
			sendByte(red);

			sendByte(prefix);
			sendByte(blue);
			sendByte(green);
			sendByte(red);
		        
		        
		    
		    sendByte(0x00);
		    sendByte(0x00);
		    sendByte(0x00);
		    sendByte(0x00);
		}
		
		
		

		/**
		 * Called every time a connection with IOIO has been established.
		 * Typically used to open pins.
		 * 
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 * @throws InterruptedException 
		 * 
		 * @see ioio.lib.util.AbstractIOIOActivity.IOIOThread#setup()
		 */
		@Override
		protected void setup() throws ConnectionLostException, InterruptedException {
			led_ = ioio_.openDigitalOutput(0, true);
			pushBtn = ioio_.openDigitalInput(12);
			light_ = ioio_.openAnalogInput(34);
			sound_ = ioio_.openAnalogInput(38);
			data = ioio_.openDigitalOutput(4);
			data.write(false);
			clk = ioio_.openDigitalOutput(5);
			clk.write(true);
			
			twi = ioio_.openTwiMaster(1, TwiMaster.Rate.RATE_100KHz, false);
			
			LSM303_write((byte)0x27, (byte)0x20);  // 0x27 = normal power mode, all accel axes on
			LSM303_write((byte)0x00, (byte)0x23);  // normal full-scale
			
			
		}

		/**
		 * Called repetitively while the IOIO is connected.
		 * 
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 * @throws InterruptedException 
		 * 
		 * @see ioio.lib.util.AbstractIOIOActivity.IOIOThread#loop()
		 */
		@Override
		public void loop() throws ConnectionLostException, InterruptedException {
			
			lightVal = light_.getVoltage();
			soundVal = sound_.getVoltage();
			
			try {

				getLSM303_accel();

			} catch (ConnectionLostException e) {

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				setColorRgb(RedVal,GreenVal,BlueVal);
			} catch (ConnectionLostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//was here: write to file
			
			
			
		}
	}

	/**
	 * A method to create our IOIO thread.
	 * 
	 * @see ioio.lib.util.AbstractIOIOActivity#createIOIOThread()
	 */
	@Override
	protected IOIOLooper createIOIOLooper() {
		return new Looper();
	}
}