import json
import pylab
import numpy as np
import array
import matplotlib.pyplot as plt
import sys
import math

f = open(sys.argv[1], 'r')

timestamp = []
ext_accel_x = []
ext_accel_y = []
ext_accel_z = []
ext_accel = []
int_accel_x = []
int_accel_y = []
int_accel_z = []
int_accel = []
light = []
sound = []

ext_thres = []
int_thres = []

for line in f:
	if "{" in line:
		data = json.loads(line)
		timestamp.append(data['time'])
		ext_accel_x.append(data['exacc'][0])
		ext_accel_y.append(data['exacc'][1])
		ext_accel_z.append(data['exacc'][2])
		ext_accel.append(math.sqrt(math.pow(data['exacc'][0],2)+math.pow(data['exacc'][1],2)+math.pow(data['exacc'][2],2)))
		int_accel_x.append(data['inacc'][0])
		int_accel_y.append(data['inacc'][1])
		int_accel_z.append(data['inacc'][2])
		int_accel.append(math.sqrt(math.pow(data['inacc'][0],2)+math.pow(data['inacc'][1],2)+math.pow(data['inacc'][2],2)))
		light.append(data['light'])
		sound.append(data['sound'])

t = np.arange(0, len(timestamp), 1)

ext_accel_thres = [0] * len(timestamp)
ext_accel_thres_sum = [0] * len(timestamp)
thres_fact = 1.8
i = 3
while i < len(timestamp):
	thres = 0
	for j in range(2,30):
		if i-j >= 0:
			if thres < math.fabs(ext_accel_x[i-j+1]-ext_accel_x[i-j]):
				thres = math.fabs(ext_accel_x[i-j+1]-ext_accel_x[i-j])
			if thres < math.fabs(ext_accel_y[i-j+1]-ext_accel_y[i-j]):
				thres = math.fabs(ext_accel_y[i-j+1]-ext_accel_y[i-j])
			if thres < math.fabs(ext_accel_z[i-j+1]-ext_accel_z[i-j]):
				thres = math.fabs(ext_accel_z[i-j+1]-ext_accel_z[i-j])
	if (ext_accel_x[i]-ext_accel_x[i-1] > thres_fact*thres) or (ext_accel_y[i]-ext_accel_y[i-1] > thres_fact*thres) or (ext_accel_z[i]-ext_accel_z[i-1] > thres_fact*thres):
		ext_accel_thres[i] = 1
		for o in range(0,20):
			if i+o < len(timestamp):
				ext_accel_thres_sum[i+o] = ext_accel_thres_sum[i-1]+1
		i += 20
	else:
		ext_accel_thres_sum[i] = ext_accel_thres_sum[i-1]
		i += 1

plt.figure()
plt.title('ext_accel_thres(_sum)')
plt.plot(t, ext_accel_x, '#FFAAAA', t, ext_accel_y, '#AAFFAA', t, ext_accel_z, '#AAAAFF', t, ext_accel, '#555555')
plt.plot(t, ext_accel_thres, 'r',t, ext_accel_thres_sum, 'b')

#plt.figure()
#plt.title('ext_accel')

plt.show()
