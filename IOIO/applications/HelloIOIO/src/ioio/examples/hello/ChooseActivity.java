package ioio.examples.hello;

import android.app.Activity;
import android.os.Bundle;

public class ChooseActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose);
	}
	
	protected void onResume() {
  		super.onResume();
	}
	
	protected void onPause() {
  		super.onPause();
	}
}
