package ioio.examples.hello;

import java.util.ArrayList;
import java.util.List;

import org.afree.chart.ChartFactory;
import org.afree.chart.AFreeChart;
import org.afree.chart.annotations.XYTextAnnotation;
import org.afree.chart.axis.NumberAxis;
import org.afree.chart.plot.PlotOrientation;
import org.afree.chart.plot.XYPlot;
import org.afree.data.xy.XYDataset;
import org.afree.data.xy.XYSeries;
import org.afree.data.xy.XYSeriesCollection;
import org.afree.graphics.geom.Font;
import org.afree.ui.TextAnchor;





import android.content.Context;
import android.graphics.Typeface;

public class GraphView extends DemoView {
	/**
     * constructor
     * @param context
     */
	
	private static float[] ext_accel_x ;
	private static float[] ext_accel_y ;
	private static float[] ext_accel_z;
	private static float[] ext_accel ;
	private static float[] int_accel_x ;
	private static float[] int_accel_y ;
	private static float[] int_accel_z ;
	private static float[] int_accel ;
	private static float[] light ;
	private static float[] sound ;
	private static ArrayList<Integer> ext_accel_thres ;
	private static ArrayList<Integer> ext_accel_thres_sum ;
	private static ArrayList<Integer> standing;
	
	private static int graphOption;
	private static int x ;
	private static float y1,y2,y3,y4,y5,y6,y7,y8,y9,y10,y14,y15;
	private static int y11,y12,y13;
	
	private static float lightLimit;
	private static float soundLimit;

	
	
    
	public GraphView(Context context, float[] ext_accel_x, float[] ext_accel_y,
    		float[] ext_accel_z, float[] ext_accel, float[] int_accel_x,
    		float[] int_accel_y,float[] int_accel_z,float[] int_accel,
    		float[] light, float[] sound, ArrayList<Integer> ext_accel_thres,
    		ArrayList<Integer>ext_accel_thres_sum, ArrayList<Integer>standing,
    		int graphOption,float lightLimit, float soundLimit) {
        super(context);
        GraphView.ext_accel_x = ext_accel_x;
        GraphView.ext_accel_y = ext_accel_y;
        GraphView.ext_accel_z = ext_accel_z;
        GraphView.ext_accel = ext_accel;
        GraphView.int_accel_x = int_accel_x;
        GraphView.int_accel_y = int_accel_y;
        GraphView.int_accel_z = int_accel_z;
        GraphView.int_accel = int_accel;
        GraphView.light = light;
        GraphView.sound = sound;
        
        GraphView.ext_accel_thres = ext_accel_thres;
        GraphView.ext_accel_thres_sum = ext_accel_thres_sum;
        GraphView.standing = standing;
        GraphView.graphOption = graphOption;
        
        GraphView.lightLimit = lightLimit;
        GraphView.soundLimit = soundLimit;
        
        final AFreeChart chart = createChart();

        setChart(chart);
    }

    /**
     * Creates a dataset.
     * @return a dataset.
     */
    private static XYSeriesCollection createDataset() {

    	XYSeries xyS1 = new XYSeries("xyS1", true, false);
    	XYSeries xyS2 = new XYSeries("xyS2", true, false);
    	XYSeries xyS3 = new XYSeries("xyS3", true, false);
    	XYSeries xyS4 = new XYSeries("xyS4", true, false);
    	XYSeries xyS5 = new XYSeries("xyS5", true, false);
    	XYSeries xyS6 = new XYSeries("xyS6", true, false);
    	XYSeries xyS7 = new XYSeries("xyS7", true, false);
    	XYSeries xyS8 = new XYSeries("xyS8", true, false);
    	XYSeries xyS9 = new XYSeries("xyS9", true, false);
    	XYSeries xyS10 = new XYSeries("xyS10", true, false);
    	XYSeries xyS11 = new XYSeries("xyS11", true, false);
    	XYSeries xyS12 = new XYSeries("xyS12", true, false);
    	XYSeries xyS13 = new XYSeries("xyS13", true, false);
    	XYSeries xyS14 = new XYSeries("xyS14", true, false);
    	XYSeries xyS15 = new XYSeries("xyS15", true, false);
    	
    	
    	for (int i = 0; i < ext_accel_x.length;i++){
    		
    		
    		xyS1.add(i, ext_accel_x[i]);
    		xyS2.add(i,ext_accel_y[i]);
    		xyS3.add(i,ext_accel_z[i]);
    		xyS4.add(i,ext_accel[i]);
    		xyS5.add(i,int_accel_x[i]);
    		xyS6.add(i,int_accel_y[i]);
    		xyS7.add(i,int_accel_z[i]);
    		xyS8.add(i,int_accel[i]);
    		xyS9.add(i,light[i]);
    		xyS10.add(i,sound[i]);
    		xyS11.add(i,ext_accel_thres.get(i));
    		xyS12.add(i,ext_accel_thres_sum.get(i));
    		xyS13.add(i,standing.get(i));
    		xyS14.add(i,lightLimit);
    		xyS15.add(i,soundLimit);
    		
    	}

    	XYSeriesCollection xySC = new XYSeriesCollection();
    	
		// 1-4
		if (graphOption == 0 || graphOption == 2 || graphOption == 3
				|| graphOption == 5) {
			xySC.addSeries(xyS1);
	    	xySC.addSeries(xyS2);
	    	xySC.addSeries(xyS3);
	    	xySC.addSeries(xyS4);

		}

		// 5-8
		if (graphOption == 1 || graphOption == 2 || graphOption == 3
				|| graphOption == 5) {
			xySC.addSeries(xyS5);
	    	xySC.addSeries(xyS6);
	    	xySC.addSeries(xyS7);
	    	xySC.addSeries(xyS8);

		}
		
		// 11,12
		if (graphOption == 3 || graphOption == 5) {
			xySC.addSeries(xyS11);
	    	xySC.addSeries(xyS12);

		}

		// 9,10
		if (graphOption == 4 || graphOption == 5) {
			xySC.addSeries(xyS9);
	    	xySC.addSeries(xyS10);
	    	xySC.addSeries(xyS14);
	    	xySC.addSeries(xyS15);
	    	

		}

		// 13
		if (graphOption == 0 || graphOption == 1 || graphOption == 2
				|| graphOption == 5) {
			xySC.addSeries(xyS13);

		}
    	
    	
    	x = ext_accel_x.length;
    	y1 =  ext_accel_x[x - 1];
    	y2 =  ext_accel_y[x - 1];
    	y3 =  ext_accel_z[x - 1];
    	y4 =  ext_accel[x - 1];
    	y5 =  int_accel_x[x - 1];
    	y6 =  int_accel_y[x - 1];
    	y7 =  int_accel_z[x - 1];
    	y8 =  int_accel[x - 1];
    	y9 =  light[x - 1];
    	y10 =  sound[x - 1];
    	y11 = ext_accel_thres.get(x-1);
    	y12 = ext_accel_thres_sum.get(x-1);
    	y13 = standing.get(x-1);
    	y14 = lightLimit;
    	y15 = soundLimit;
    	
    	
    	
    	

    	return xySC;
    }

    /**
     * Creates a sample chart.
     * @param dataset the dataset.
     * @return A sample chart.
     */
    private static AFreeChart createChart() {
        XYDataset dataset = createDataset();
        AFreeChart chart = ChartFactory.createXYLineChart(
        		"Data Plot",
                "X Value - Time (seconds)",
                "Y Value - Value",
                dataset,
                PlotOrientation.VERTICAL,
                false,
                true,
                false);
        XYPlot plot = (XYPlot) chart.getPlot();
        NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
        domainAxis.setUpperMargin(0.2);
        
        
       
        
        // add some annotations...
        
        XYTextAnnotation annotation = null;
        Font font = new Font("SansSerif", Typeface.NORMAL, 12);
        
		// 1-4
		if (graphOption == 0 || graphOption == 2 || graphOption == 3
				|| graphOption == 5) {
			annotation = new XYTextAnnotation("Ext X", x, y1);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);

	        annotation = new XYTextAnnotation("Ext Y", x, y2);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);

	        annotation = new XYTextAnnotation("Ext Z", x, y3);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
	        
	        annotation = new XYTextAnnotation("Ext", x, y4);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
	        
	        

		}

		// 5-8
		if (graphOption == 1 || graphOption == 2 || graphOption == 3
				|| graphOption == 5) {
			
			annotation = new XYTextAnnotation("Int X", x, y5);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
	        
	        annotation = new XYTextAnnotation("Int Y", x, y6);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
	        
	        annotation = new XYTextAnnotation("Int Z", x, y7);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
	        
	        annotation = new XYTextAnnotation("Int", x, y8);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
			

		}

		// 11,12
		if (graphOption == 3 || graphOption == 5) {
			
			annotation = new XYTextAnnotation("Ext Thres", x, y11);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
	        
	        annotation = new XYTextAnnotation("Ext Thres Sum", x, y12);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
			

		}

		// 9,10
		if (graphOption == 4 || graphOption == 5) {
			
			annotation = new XYTextAnnotation("Light", x, y9);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
	        
	        annotation = new XYTextAnnotation("Sound", x, y10);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
	        
	        annotation = new XYTextAnnotation("Light Limit", x, y14);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
	        
	        annotation = new XYTextAnnotation("Sound Limit", x, y15);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);
	        
	        
		

		}

		// 13
		if (graphOption == 0 || graphOption == 1 || graphOption == 2
				|| graphOption == 5) {
			annotation = new XYTextAnnotation("Standing", x, y13);
	        annotation.setFont(font);
	        annotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
	        plot.addAnnotation(annotation);

		}
        
        
        

        return chart;
    }

}
