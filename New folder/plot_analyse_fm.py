import json
import pylab
import numpy as np
import array
import matplotlib.pyplot as plt
import sys
import math

f = open(sys.argv[1], 'r')

timestamp = []
ext_accel_x = []
ext_accel_y = []
ext_accel_z = []
ext_accel = []
int_accel_x = []
int_accel_y = []
int_accel_z = []
int_accel = []
light = []
sound = []

ext_thres = []
int_thres = []

positionBack = 0;
positionSide = 0;
positionStom = 0;

for line in f:
	if "{" in line:
		data = json.loads(line)
		timestamp.append(data['time'])
		ext_accel_x.append(data['exacc'][0])
		ext_accel_y.append(data['exacc'][1])
		ext_accel_z.append(data['exacc'][2])
		ext_accel.append(math.sqrt(math.pow(data['exacc'][0],2)+math.pow(data['exacc'][1],2)+math.pow(data['exacc'][2],2)))
		int_accel_x.append(data['inacc'][0])
		int_accel_y.append(data['inacc'][1])
		int_accel_z.append(data['inacc'][2])
		int_accel.append(math.sqrt(math.pow(data['inacc'][0],2)+math.pow(data['inacc'][1],2)+math.pow(data['inacc'][2],2)))
		light.append(data['light'])
		sound.append(data['sound'])
		if data['position'] == 0:
			positionBack+=1
		elif data['position'] == 1:
			positionSide+=1
		else:
			positionStom+=1

t = np.arange(0, len(timestamp), 1)

ext_accel_thres = [0] * len(timestamp)
thres_fact = 5
i = 3
while i < len(timestamp):
	thres = 0
	mean = 0
	mean_c = 0
	for j in range(2,60):
		if i-j >= 0:
			mean += math.fabs(ext_accel_x[i-j+1]-ext_accel_x[i-j])
			mean += math.fabs(ext_accel_y[i-j+1]-ext_accel_y[i-j])
			mean += math.fabs(ext_accel_z[i-j+1]-ext_accel_z[i-j])
			mean_c += 3

	thres = mean/mean_c
	if (ext_accel_x[i]-ext_accel_x[i-1] > thres_fact*thres) or (ext_accel_y[i]-ext_accel_y[i-1] > thres_fact*thres) or (ext_accel_z[i]-ext_accel_z[i-1] > thres_fact*thres):
		ext_accel_thres[i] = 1
	i += 1

ext_accel_thres_sum = [0] * len(timestamp)
i = 1
j = -100
flag = 0
while i < len(timestamp):
	ext_accel_thres_sum[i] = ext_accel_thres_sum[i-1]
	if (ext_accel_thres[i] == 1) and (i-j >= 100):
		if flag != 0:
			ext_accel_thres[j+1] = -1
		ext_accel_thres[i] = 1
		flag = 1
		j = i
		ext_accel_thres_sum[i] += 1
	elif (ext_accel_thres[i] == 1) and (i-j < 100):
		if flag == 0:
			ext_accel_thres[j] = 0
		ext_accel_thres[i] = -1
		flag = 0
		j = i
	i += 1
if flag != 0:
	ext_accel_thres[j+1] = -1
		

plt.figure()
plt.title('ext_accel_thres and ext_accel_thres_sum')
plt.plot(t, int_accel_x, '#FFAAAA', t, int_accel_y, '#AAFFAA', t, int_accel_z, '#AAAAFF', t, int_accel, '#555555')
plt.plot(t, ext_accel_x, '#FFAAAA', t, ext_accel_y, '#AAFFAA', t, ext_accel_z, '#AAAAFF', t, ext_accel, '#555555')
plt.plot(t, ext_accel_thres, 'r',t, ext_accel_thres_sum, 'b')

standing = [0] * len(timestamp)
percent = 0.5
for i in range(1, len(timestamp)-1):
	if (math.fabs(int_accel_y[i-1]) > 9.81*percent) and (math.fabs(int_accel_y[i]) > 9.81*percent) and (math.fabs(int_accel_y[i+1]) > 9.81*percent):
		standing[i] = 1

plt.figure()
plt.title('standing')
plt.plot(t, int_accel_x, '#FFAAAA', t, int_accel_y, '#AAFFAA', t, int_accel_z, '#AAAAFF', t, int_accel, '#555555')
plt.plot(t, ext_accel_x, '#FFAAAA', t, ext_accel_y, '#AAFFAA', t, ext_accel_z, '#AAAAFF', t, ext_accel, '#555555')
plt.plot(t, standing, 'r')

print 
print "Detected the following events:"
i = 20
inbed = 0
count_rem = 0
index_entrance = -1
index_exit = -1
count_gotups = 0
noise_limit = "No"
light_limit = "No"
quality = 0
while i < len(timestamp)-20:
	if ext_accel_thres[i] == 1:
		j = i+1
		while j < len(timestamp)-20:
			if ext_accel_thres[j] == -1:
				if standing[i-15] > standing[j+15]:
					if index_entrance == -1:
						index_entrance = i
					else:
						count_gotups += 1
					print "\tBed entrance: " + str(timestamp[i])
					inbed = 1
				elif standing[i-15] < standing[j+15]:
					index_exit = i
					print "\tBed exit: " + str(timestamp[i])
					inbed = 0
				else:
					if j-i == 1:
						print "\tUnknown event (probably false-detection): " + str(timestamp[i])
					else:
						if standing[i-15] == 1 and inbed == 0:
							print "\tMade the Bed: " + str(timestamp[i])
						elif standing[i-15] == 0 and inbed == 1:
							count_rem += 1
							print "\tREM-phase: " + str(timestamp[i])
						else:
							print "\tDid something wired: " + str(timestamp[i])
				break
			j += 1
		i = j+1
	if sound[i] > 0.5:
		print "\tNoise limit exceded: " + str(timestamp[i])
		noise_limit = "Yes"
		quality -= 1
 	if light[i] > 2.5:
		print "\tLight limit exceded: " + str(timestamp[i])
		light_limit = "Yes" 
		quality -= 1
	i += 1

print

if math.floor(timestamp[index_entrance]/1000000) != math.floor(timestamp[index_exit]/1000000):
	duration_h = 24
else:
	duration_h = 0
duration_h += (int)( math.floor(timestamp[index_exit]/10000) - math.floor(timestamp[index_exit]/1000000)*100 )
duration_h -= (int)( math.floor(timestamp[index_entrance]/10000) - math.floor(timestamp[index_entrance]/1000000)*100 )
duration_m = 0
duration_m += (int)( math.floor(timestamp[index_exit]/100) - math.floor(timestamp[index_exit]/10000)*100 )
duration_m -= (int)( math.floor(timestamp[index_entrance]/100) - math.floor(timestamp[index_entrance]/10000)*100 )
if duration_m < 0:
	duration_m = 60 + duration_m
	duration_h -= 1


print "Time in back position:"
print str(positionBack) + " seconds"
print "Time in side postion:"
print str(positionSide) + " seconds"
print "Time in stomach position:"
print str(positionStom) + " seconds"

print "Sleep quality parameters:"
print "\tDuration: " + str(duration_h) + "h " + str(duration_m) + "min" 
print "\tNumber of REM-cycles: " + str(count_rem)
print "\tNumber of Got-ups: " + str(count_gotups)
print "\tNoise limit exceded: " + noise_limit
print "\tLight limit exceded: " + light_limit

print


if duration_h >= 8:
	quality += 1
if duration_h >= 7:
	quality += 1
if count_rem >= 3:
	quality += 1
if count_rem >= 5:
	quality += 1
if count_gotups > 1:
	quality -= 1

quality_str = "Poor"
if quality >= -1:
	quality_str = "Okay"
if quality > 0:
	quality_str = "Good"
print "Estimated Sleep Quality: " + quality_str

print


plt.show()
